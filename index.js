const http=require('http')
const fs=require('fs')
const path=require('path')
const uuid=require('uuid')
const host='localhost'
const port=3000
const server=http.createServer((req,res)=>{
    if(req.url==='/html'){
        res.setHeader('content-type','text/html')
    fs.readFile(path.join(__dirname,'index.html'),'utf-8',(err,data)=>{
        if(err){
            res.writeHead(404,'error')
            return res.end('error')
        }
        else{
            res.writeHead(200,'text/html')
            return res.end(data)
        }
    })
}
else if(req.url==='/json' && req.method=='GET'){
    res.setHeader('content-type','application/json')
    fs.readFile(path.join(__dirname,'index.json'),'utf-8',(err,data)=>{
        if(err){
            res.writeHead(404,'error')
            return res.end('error')
        }
        else{
            res.writeHead(200,'application/html')
            return res.end(data)
        }
    })
}
else if(req.url==='/uuid' && req.method=='GET'){
    
    res.setHeader('content-type','application/json')
    return res.end(JSON.stringify(`{uuid:${uuid.v4()}}`))
}
else if(req.url.includes('/status/') && req.method=='GET'){
    res.setHeader('content-type','application/json')
    let statusarray=req.url.split('/')
    let statuscode=parseInt(statusarray[statusarray.length-1])
    if(typeof(statuscode)==='number'){
        return res.end(`${statuscode},${http.STATUS_CODES[statuscode]}`)
    }
}else if(req.url.includes('/delay/') && req.method=='GET'){
    res.setHeader('content-type','application/json')
    let delayarray=req.url.split('/')
    let statuscode=200
    let delaytime=delayarray[delayarray.length-1]
    setTimeout(()=>{
        res.write(`${statuscode},${http.STATUS_CODES[statuscode]}`)
        return res.end()
    },delaytime*1000)
}else{
    return res.end('enter valid url')
}

}).listen(port,host,()=>{
    console.log(`server is running at http://${host}:${port}`)
})